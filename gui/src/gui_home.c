/*
 * Copyright (c) 2021 Coopermaq
 * All rights reserved
 * Author: Zander Cittadin
 */
#include <stdio.h>
#include "lvgl.h"
#include "ftoa.h"
#include "accelerometer.h"
#include "log.h"
#include "gui_home.h"

static void home_refresh(lv_task_t * task);

lv_task_t * home_refresh_task;

lv_obj_t * home_page;
lv_obj_t * label_angle;
lv_obj_t * label_addr;
lv_obj_t * gauge_angle;

accel_data_t accel_data;

char str_angle[4] = "";
char str_addr[6] = "";

void home_screen() {

    uint8_t addr = accel_get_dev_addr();
    if(addr != 0x00) {
        uart6_printf("\n0x%02X\n", addr);
        sprintf(str_addr, "0x%02X", addr);
    } else {
        uart6_printf("\nINIT_ERR\n");
        strcpy(str_addr, "ERRO");
    }

    static lv_style_t addr_style;
    lv_style_init(&addr_style);
    lv_style_set_text_color(&addr_style, LV_STATE_DEFAULT, LV_COLOR_RED);

    static lv_style_t angle_style;
    lv_style_init(&angle_style);
    lv_style_set_text_color(&angle_style, LV_STATE_DEFAULT, LV_COLOR_BLUE);

    home_page = lv_obj_create(lv_scr_act(), NULL);
    lv_obj_set_size(home_page, 800, 480);
    lv_obj_align(home_page, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0); 
    
    lv_obj_t * label = lv_label_create(home_page, NULL);
    lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 10);
    lv_label_set_text(label, "END. I2C:");

    label_addr = lv_label_create(home_page, NULL);
    lv_label_set_recolor(label_addr, true);
    lv_obj_align(label_addr, NULL, LV_ALIGN_IN_TOP_LEFT, 160, 10);
    lv_label_set_text(label_addr, str_addr);
    lv_obj_add_style(label_addr, LV_OBJ_PART_MAIN, &addr_style);

    label = lv_label_create(home_page, NULL);
    lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 50);
    lv_label_set_text(label, "ANGULO:");

    label_angle = lv_label_create(home_page, NULL);
    lv_obj_align(label_angle, NULL, LV_ALIGN_IN_TOP_LEFT, 160, 50);
    lv_label_set_text(label_angle, "0.00");
    lv_obj_add_style(label_angle, LV_OBJ_PART_MAIN, &angle_style);

    static lv_color_t needle_colors[1];
    needle_colors[0] = LV_COLOR_BLUE;

    gauge_angle = lv_gauge_create(home_page, NULL);
    lv_gauge_set_needle_count(gauge_angle, 1, needle_colors);
    lv_obj_set_size(gauge_angle, 350, 350);
    lv_obj_align(gauge_angle, NULL, LV_ALIGN_CENTER, 0, 0);

    lv_gauge_set_range(gauge_angle, -90.0, 90.0);

    lv_gauge_set_critical_value(gauge_angle, 54);

    lv_gauge_set_value(gauge_angle, 0, 0);

    home_refresh_task = lv_task_create(home_refresh, 100, LV_TASK_PRIO_LOW, NULL);
}

static void home_refresh(lv_task_t * task) {
    accel_get_data(&accel_data);
    uart6_printf("%4.2f, %4.2f, %4.2f, %4.2f\n", accel_data.x_acc, accel_data.y_acc, accel_data.z_acc, accel_data.angle);
    lv_label_set_text(label_angle, ftoa(accel_data.angle, str_angle, 2));
    lv_gauge_set_value(gauge_angle, 0, accel_data.angle);
}