CSRCS += src/bma4.c
CSRCS += src/bma456.c
CSRCS += src/bma4_common.c

DEPPATH += --dep-path Drivers/bma4/src
VPATH += :Drivers/bma4/src

CFLAGS += -IDrivers/bma4/inc
