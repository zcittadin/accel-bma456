CSRCS += src/log.c
CSRCS += src/dwt_stm32_delay.c
CSRCS += src/ftoa.c
CSRCS += src/ssd1963.c
CSRCS += src/touchscreen.c

DEPPATH += --dep-path Drivers/coopermaq/src
VPATH += :Drivers/coopermaq/src

CFLAGS += -IDrivers/coopermaq/inc
