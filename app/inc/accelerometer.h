/*
 * Copyright (c) 2021 Coopermaq
 * All rights reserved
 * Author: Zander Cittadin
 */
#ifndef ACCEL_H_
#define ACCEL_H_

#define ACCEL_I2C_ADDR      (0x18 << 1)
#define GRAVITY_EARTH       (9.80665f)

typedef struct {
    float x_acc;
    float y_acc;
    float z_acc;
    float angle;
} accel_data_t;

uint8_t accel_get_dev_addr(void);
uint16_t accel_config(void);
uint16_t accel_init(void);
void accel_stop(void);
void accel_get_data(accel_data_t *data);

#endif /* ACCEL_H_ */
