/*
 * Copyright (c) 2021 Coopermaq
 * All rights reserved
 * Author: Zander Cittadin
 */
#include <stdio.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "FreeRTOS.h"
#include "bma456.h"
#include "bma4_common.h"
#include "accelerometer.h"

static void v_task_accel(void * pvParameters);
static uint16_t accel_read(void);
static float lsb_to_ms2(int16_t val, float g_range, uint8_t bit_width);

TaskHandle_t accel_task_handle = NULL;
SemaphoreHandle_t accel_mutex = NULL;

struct bma4_dev bma;
struct bma4_accel sens_data = { 0 };
struct bma4_accel_config accel_conf = { 0 };
static accel_data_t accel_data;

uint16_t int_status = 0;

uint8_t accel_get_dev_addr() {
    return bma4_get_dev_addr();
}

uint16_t accel_config() {
    int8_t rslt;
    rslt = bma4_interface_selection(&bma);
    if(rslt == BMA4_OK) {
        rslt = bma456_init(&bma);
        if(rslt == BMA4_OK) {
            rslt = bma456_write_config_file(&bma);
            if(rslt == BMA4_OK) {
                rslt = bma4_set_accel_enable(BMA4_ENABLE, &bma);
                accel_conf.odr = BMA4_OUTPUT_DATA_RATE_25HZ;
                accel_conf.range = BMA4_ACCEL_RANGE_2G;
                accel_conf.bandwidth = BMA4_ACCEL_NORMAL_AVG4;
                accel_conf.perf_mode = BMA4_CIC_AVG_MODE;
                rslt = bma4_set_accel_config(&accel_conf, &bma);
                if(rslt == BMA4_OK)
                    rslt = bma456_map_interrupt(BMA4_INTR1_MAP, BMA4_DATA_RDY_INT, BMA4_ENABLE, &bma);
            }
        }
    }
    return rslt;
}

uint16_t accel_init() {
    if(!accel_mutex)
        accel_mutex = xSemaphoreCreateMutex();
    if(accel_task_handle)
        return HAL_OK;
    BaseType_t ret;
    ret = xTaskCreate(v_task_accel, "v_task_accel", 1024, NULL, tskIDLE_PRIORITY, &accel_task_handle);
    if (ret != pdPASS) {
        return HAL_ERROR;
    }
    return HAL_OK;
}

void accel_stop() {
    if (accel_task_handle) {
        vTaskDelete(accel_task_handle);
        accel_task_handle = NULL;
    }
}

void v_task_accel(void * pvParameters) {
    while(1) {
        accel_read();
        osDelay(1);
    }
}

void accel_get_data(accel_data_t *data) {
    if (xSemaphoreTake(accel_mutex, pdMS_TO_TICKS(1)) != pdTRUE) {
        return;
    }
    data->x_acc = accel_data.x_acc;
    data->y_acc = accel_data.y_acc;
    data->z_acc = accel_data.z_acc;
    data->angle = accel_data.angle;
    xSemaphoreGive(accel_mutex);
}


static uint16_t accel_read() {
    if (xSemaphoreTake(accel_mutex, pdMS_TO_TICKS(1)) != pdTRUE) {
        return HAL_BUSY;
    }
    int8_t rslt;
    rslt = bma456_read_int_status(&int_status, &bma);
    if ((rslt == BMA4_OK) && (int_status & BMA4_ACCEL_DATA_RDY_INT)) {
        /* Read the accel x, y, z data */
        rslt = bma4_read_accel_xyz(&sens_data, &bma);
        if (rslt == BMA4_OK) {
            /* Converting lsb to meter per second squared for 16 bit resolution at 2G range */
            accel_data.x_acc = lsb_to_ms2(sens_data.x, 2, bma.resolution);
            accel_data.y_acc = lsb_to_ms2(sens_data.y, 2, bma.resolution);
            accel_data.z_acc = lsb_to_ms2(sens_data.z, 2, bma.resolution);
            accel_data.angle = atan(-1 * accel_data.y_acc / sqrt(pow(accel_data.z_acc, 2) + pow(accel_data.x_acc, 2))) * 180 / M_PI;
        }
    }
    xSemaphoreGive(accel_mutex);
    return rslt;
}

static float lsb_to_ms2(int16_t val, float g_range, uint8_t bit_width) {
    float half_scale = ((float)(1 << bit_width) / 2.0f);
    return (GRAVITY_EARTH * val * g_range) / half_scale;
}